import Vue from 'vue'
import Vuex from 'vuex'



Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    todoList: [],

  },
  getters: {
    getTasks: (state) => state.todoList,
  },
  mutations: {
    setTasks: (state, task) => (state.todoList = task),
  },
  actions: {
    async createTask({ commit }, task) {
        commit('setTasks', task)
    },
  },
  modules: {

  }
})


