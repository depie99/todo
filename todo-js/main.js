const newTaskInput = document.getElementById("new-task");
const addTaskButton = document.getElementById("add-task");
const taskList = document.getElementById("task-list");

// Create new task item
function createTaskItem(taskText) {
    const taskItem = document.createElement("li");

    const checkBox = document.createElement("input");
    checkBox.type = "checkbox";

    const taskSpan = document.createElement("span");
    taskSpan.innerText = taskText;

    const deleteButton = document.createElement("button");
    deleteButton.innerText = "Delete";

    // Almost Forgot! Delete task item :)
    deleteButton.addEventListener("click", function() {
        taskItem.remove();
    });

    taskItem.appendChild(checkBox);
    taskItem.appendChild(taskSpan);
    taskItem.appendChild(deleteButton);

    return taskItem;
}

// OKK! Add new task to list
addTaskButton.addEventListener("click", function() {
    const newTaskText = newTaskInput.value;
    if (newTaskText) {
        const taskItem = createTaskItem(newTaskText);
        taskList.appendChild(taskItem);
        newTaskInput.value = "";
    }
});

